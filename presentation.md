---
title:
- "Programmes de recherche du Metalab 2022-2025"
author:
- Emmanuel Durand, Nicolas Bouillot
institute:
- Société des Arts Technologiques [SAT]
theme:
- default
date:
- 9 février 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

## Structure de la présentation

* Introduction et nouveaux éléments communs
* Téléimmersion hybride, reproductible et durable (2022-2025)
* Augmentation multisensorielle d’espaces pour l’immersion sociale (2022-2025)
* Discussions

# Introduction et éléments communs

## 

* Continuité des travaux du Metalab: dispositifs pour l'immersion, téléprésence et outils pour la création et le déploiement
* Les cas d'usages et partenariats des précédents programmes motivent les nouveaux travaux 

## Rédaction des programmes 

* Erratum : le Centre PHI est partenaire uniquement de "Téléimmersion hybride, reproductible et durable"
* Ajout de glossaires en cours

## Nouveaux éléments communs

* Nouvelle direction de l'innovation, structuration du pipeline de recherche et valorisation
* Reconnaissance de la [SAT] comme centre de recherche public
* Programme de résidence R&D à la SAT
* MITACS dans le processus de partenariats

## Résultats attendus des programmes

*  Preuves de concept : cas d'usage, méthodes et technologies
*  Publication et démonstration de prototypes
*  Publication de code pour amélioration et extension des outils du Metalab
*  Documentation
*  Communication scientifique et grand public

# Téléimmersion hybride, reproductible et durable (2022-2025)

## Expériences téléimmersives hybrides

![](img/MotusDomum_remote.jpg){width=40%} ![](img/MotusDomum_local.jpg){width=40%}

Exemples de cas d'usage : œuvre artistique, télécollaboration, conférence, diffusion participative

## Dispositifs et expériences pérennes

![](img/Mirador_complete.jpg){width=40%} ![](img/Audiodice_lab.jpg){width=40%}

Exemples de cas d'usage : installation spontanée, reproduction des résultats de recherche, conservation d'œuvre, tournée

## Problématique 1 - Expériences hybrides en téléprésences

Enjeux liés aux expériences hybrides : hétérogénéité des dispositifs et interactions sociales.

* Mieux supporter les échanges sociaux dans les environnements de la téléimmersion
* Améliorer la représentation virtuelle et des participant.e.s

## Problématique 2 - Pérennité et reproductibilité numérique

Les technologies courantes sont peu durables et peu adaptables.

* Améliorer l’interopérabilité
* Prototyper des plateformes matérielles légères
* Expérimenter des processus permettant de rejouer des installations

## Problématique 3  -  Empreinte des expériences téléimmersives

L’impact de la quantité de matériel nécessaire pour la téléimmersion est important.

* Rendre compatible nos outils avec des plateformes légères
* Expérimenter l’usage de technologies *low tech* pour la téléimmersion

## Partenaires

* Orchestre Symphonique de Montréal
* Centre PHI
* Créo
* Linux Audio Conférence

## Déroulement

![](img/deroulement-depl.jpg){width=100%}

# Augmentation multisensorielle d’espaces pour l’immersion sociale (2022-2025)

## Augmentation multisensorielle

![](img/Haptic_floor_control.png){width=40%} ![](img/varays_live.png){width=40%}

Exemples de cas d'usage : œuvre artistique impliquant des sensorialités variées, simulation architecturale, enseignement

## Immersion sociale

![](img/icsa_baleinopolis_composition.jpg){width=40%} ![](img/LivePose_Molecule.png){width=40%} 

Exemples de cas d'usage : installations muséales et spectacles interactifs

## Problématique 1 - Création immersive multisensorielle

Les outils de création de chaque modalité (vision, audition ...) s'intègrent mal ensemble.

* Réduire le temps d’intégration des travaux issus d’une même équipe
* Augmenter l’engagement des participants

## Problématique 2 - Immersion sociale interactive

La captation des gestes de chaque participant.e d’une expérience est difficilement accessible.

* Améliorer le sentiment de coprésence des participants
* Améliorer les interactions en direct entre participant.e.s et avec les espaces
* Étudier et intégrer des méthodes d’interaction de groupe

## Problématique 3 - Simulation et augmentation spatiale

Les créations s’adaptent difficilement aux caractéristiques des espaces.

* Adapter l'expérience aux spécificités des espaces
* Améliorer la simulation et l’enrichissement sensoriel des espaces

## Partenaires

* Ossia
* EnsAD - Reflextive Interaction
* Les 7 Doigts (LAB7)
* Eastern Bloc & Ada X
* IDMIL

## Déroulement

![](img/deroulement-multi.jpg){width=100%}

# Discussion

## 

# Vidéos

## OSM

<iframe src="https://player.vimeo.com/video/497051293" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## Expériences hybrides

<iframe src="https://player.vimeo.com/video/544570900" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## Expériences hybrides

<iframe src="https://player.vimeo.com/video/549433773" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

# Écosystème d'outils du Metalab

##

![](assets/metalab-ecosystem.png)

# Observation sur les partenariats associés

##

![](img/partenaires.png){width=90%}

# Retour sur ICSA et SAV+R (2019-2022)

## ICSA

![](./assets/moving_projection.png){width=45%}
![](./assets/Klimt_expo_immersive.jpg){width=45%}

**Immersion Collaborative Spontanée et Adaptative**

## ICSA - Objectifs

* Permettre l'immersion collective en tout lieu
* Simplifier l'adaptation et création des expériences au lieu physique

## ICSA - quelques résultats marquants

* Création du logiciel LivePose pour le suivi de groupe de personnes en direct
* Développement du prototype matériel Mirador
* Création de Satellite, une plateforme de diffusion de contenu immersif en directe
* Collaboration avec l'industrie

## SAV+R

![](./assets/Bretez-David.png){width=45%}
![](./assets/Bretez-Michal.png){width=45%}

**Simulation Acoustique Volumétrique en AR/MR/XR**

## SAV+R - Objectifs

* Explorer la simulation d'acoustique dans la création d'expériences immersives
* Augmenter la sensorialité des espaces immersifs

## SAV+R - quelques résultats marquants

* Prototype de navigation dans l'OSM
* Prototype matériel Audiodice
* Création du logiciel de simulation acoustique vaRays
* Phase de pré-commercialisation du plancher haptique
